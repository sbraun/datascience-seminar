# Datascience seminar

## Preamble

You'll need to bring your device to this seminar!
In preparation please install pycharm and python 3.8 (or larger).
You can use another IDE you're familiar with, but we'll discuss some IDE "tricks" that the other IDE might not support.

If you're unfamiliar with python or need a refresher, please prepare the following courses for the first sprint:

- https://www.kaggle.com/learn/intro-to-programming
- https://www.kaggle.com/learn/python

Feel free to contact us in case of questions or problems.

## Sprint 1

### Project introduction and outline

This course will teach you the basics of how to process and validate your data, as well as the basics and leassons learned when working with data. We'd like to provide a hands-on experience by having a practial course where you need to process data provided by us.
Additionally, you will learn the basics of good programming paradigms and version control.

The provieded data used in this couse was generated using a hackrf. The hackrf is a software defined radio that can send or process radio signals.
The measurement was conducted during a scientific project. The hackrf was used to measure the nose level as well as a periodically chaning signal generated automatically by a WiFi access point.
In the practical part of this course, you will use one dataset to create a dataanalysis. Hereby, you will cross many datascience topics.

The data from `data/measurement.csv` was generated using [hackrf\_sweep](https://hackrf.readthedocs.io/en/latest/hackrf_sweep.html) and was stored as a csv file.
The csv file did not contain a header, thus documentation is needed to understand the rows of the measurement: The first two colums are the timestamp of the measurement, follwed by the low- and high frequency, the bin-width and the number of samples.
The rest columns contain the activity of the frequency in decibel, starting with the first bin reaching from `low frequency` to `low frequency + bin-width`.

The couse practical course will consist of:
- Reading and preprocessing the data
- Plotting the processed data (all)
- Feature extraction (from a column) and plotting
- Visualisation of the aggregated data (all features of all columns)

We'd recommend to use an entrypoint for example a module 'seminar/main.py' that uses your modules you've created to fullfill the tasks presented in this course.
`seminar.main` as well as `seminar.preprocessing` are already created in the git project. However, their programm logic is not implemented yet.
We'd recommend to create create more modules that fulfill a single task according to the project outline, i.e. `seminar.plot`, `seminar.extration`, and `seminar.final`.
Use `seminar.main` to import and call your modules.

Task: Write your `main` programm in pseudocode and the call and return parameters of your modules. Keep in mind to specify filenames (when images are crated) as well.

### Toolchain (Numpy, Pandas, Matplotlib, Scipy)

You often read that python is slow compared to precompiled programms.
This is true, however there is a good reason behind this. Python is object oriented and every operation first includes a type-checking, which costs time.
Thus, a mathematical operations `3 + 2` are carried out slow.

However, there is a simple solution: You can use c-type datastructures and (mathematical) operations that are CPU optimized (and precompiled) using libraries.
A broadly used library is called [numpy](https://numpy.org/) (short for numerical python). Numpy, offers the majority of mathematical operations and provides a datastructure that is executed comparable to (fast) c-code.
Moreover, it also provdes a intuitive API and is easy to use.

You can also read and write data with numpy, however it is not specialized for these tasks. Thus, it is recommended to use a second library: [pandas](https://pandas.pydata.org/).
You can easily read and save multiple file formats with pandas, for example excel and csv, which are both common file formats that contain data.
Pandas is build ontop of numpy and makes data manipulation easier than numpy, with the drawback of an additional overhead.

With math and datahandling covered, the next topic is visualisation. We use [matplotlib](https://matplotlib.org/) for data visualization.
It consists of many tools to visualize your data in graphs. It offers a large example-library where examples are given and provide you with a good starting point.

Often math is not enough, but you need algorithms, that are typically only historically covered by numpy. Amongs others, [scipy](https://www.scipy.org/) is a library that provides algorithms to work with data.
We'll use a `curve_fit` for a feature extraction.

All these tools have a really good documentation! Make yourself familiar with these tools!

### Gitlab Introduction

* https://i40.fh-aachen.de/courses/datascience/git/
* https://nvie.com/posts/a-successful-git-branching-model/

### Pycharm setup

Live Demo (or see best practices)

### Python best practices

* https://i40.fh-aachen.de/courses/datascience/python-best-practices

## Homework A: Waterfall diagram of a dataset

- Fork and clone this repository
- Setup your IDE according to the best practices (static code analysis!)
- Write a programm that uses `pandas` to read the data from `data/measurement.csv`. Use the parameter `prefix` because the data has no header.
- Preprocess the data included in the dataset into a second dataset that has one column containina all measurements taken at the same time. Therefore, label the rows according to their frequency-bins, skip duplicated datarows as well as datarows that don't contain the full frequency spectrum. Finally, combine the date and time into a single `datetime` index.
- Use `matplotlib` (`imshow`) and the preprocessed data to create a waterfall diagram.

Hint: It can save time, if you save the aggregated data into a file and use this file as a starting point.
Keep in mind that saving and reading data in a binary format is more efficient than `csv` or `xls`.

![Waterfall diagram](images/waterfall.png "Waterfall diagram")

## Sprint 2

### Lessons Learned "Pandas"

Live Discussion

* pandas is handy in comparision with numpy
* pandas can do powerfull aggregations of indexed data
* pandas supporty multi-indexed datasets
* We'd recommend to use pandas with index columns, the index is kept then a subset is viewed.
* It can be complicated to understand when pandas is returning a view on the dataset-instance or a copy of the data.
* There are issues, that in 3rd party applications (sklearn) numpy arrays perform magnitudes faster then pandas dataframes!

### Concurrency with Python

Concurrency is the ability to work on a task on parallel. It improves perceived responsiveness and speed.
There are three types of problems in computer science:

- Problems that can be solved on one CPU core
- Problems that can be solved on one PC (multiple threads and processes)
- Problems that require a distributed processing (more than one node)

Martelli’s observation: As time goes on, the second category becomes less common and relevant. Single cores become more powerful, while datasets grow larger.
Thie means that unter the first class fall many problems and they don't require concurrent approaches to be solved.
Many other problems have scaling, so that they don't fall into the second category, but rather require a compute-cluster to be tackled.
The minority of problems is viable for execution that used the resources of only a single pc.
Thus using concurrecy to reduce process times can be seen as a niece solution.

Python has a lock for its internal shared global state. This lock is called global interpreter lock (GIL) and means that only one thread is used by python.
For I/O bound applications, the GIL does not present much of an issue.
For CPU bound applications, using threading makes the application speed worse.
This means that multi-processing can be used to gain more CPU cycles.

The difference between threads and processes:
A process as a defined input and outout and does not share memory, they are independent from each other.
It has high communication costs, as the inputs and outputs are pickled and than comminicated with the process.
A thread shares the memory with all threads started by the main process.
This shared state requires to the management of race conditions.

Multithreadded applications seem easier, than using processes, but you need to consider thread-savety - and need to ensure that a thread does not write data that is read by another thread.
The reson behind this is that threads switch preemptively.
This is convenient because there is no need to add explicit code to cause a task switch, however the tasks switch can occur anytime.
Therefore, you need to use locks to guars critical sections.
When using locks. the thread waits on a lock to be released and thus it reduces the overall performance, as locks are not cheap to aquire.
If this is not taken into account, a programm might be functioning like expected until a task, that does not take thread savety into account, is creating a hard to reproduce bug.

Thus said, threads are often used to parallelize IO operations and they are often scaled to a multiple (two or three times) of the number of available processors.
Multiprocessing is often used to parallelize CPU limited operations and the number of processes should not be higher than the number of cores on the system.

Another solution to parallelize IO tasks is the useage of asyncio.
Async only switches cooperatively - a developer needs to add explicit code ``yield`` or ``await`` to cause a task switch.
Now the programm controls when task switch can occur, and locks and other synchronization are no longer needed.
The programm creates events, that are usually processed in a timely manner by a single process.
The event usually triggered by IO or timeouts. As long as no event is processed, the event queue can accept new events.
A `await` or 'yield' is used to schedule a new event and process the next event in the queue.
This parallelizes a programm just by efficently using the resources awailable.
The cost task switches is very low - even lower than calling a function. Thus asyncio is very cheap.
However, non-blocking versions of "everything" are required to fulfill a task, which requires a huge (parallel) ecosystem of support tools and increases the learning curve.

One famous example is to download many datasets, for example 1000, from the web. Each request takes about 120ms, thus a python programm without concurrency will take two minutes (120ms * 1000). Using more processes or threads will reduce the time roughly by the number of processes or threads used. Using asyncio, the data can be collected even faster, as it it not limites by the number of processes.

In datascience applications asyncio can be used when you have many IO-Operations, like datasets that need to be downloaded.
Threadding is dangerous and should be avoided and multiprocessing can sppedup your programm by utializing the whole PC.
In our case, multiprocessing can be used to paralellize the plot generation.

### Feature extraction

There are automated approaches to extract features from dataset. These approaches are used to categorize often multidimensional datasets into a number of categories.
These approaches are covered by libraries like [scipy](https://www.scipy.org/), which is a topic later in this course.
First, we focus on the manual feature extraction, which starts by looking at the data:

* Can you see special behaviour?
* What is good data and what data is poluted?
* Does your data behave like your model or a physical model?
* Does the data proof or disproof your thesis?

Looking at the waterfall diagramm, we see all the data and we can features.
We are interested in the signal to noise ratio over the measured frequencies.
Just by looking at the data we see artefacts, areas with much polution and areas where our transmitter seems to be the only source.

This leads to the following questions:
- What is the noise-level?
- How high is the signal?
- How can artefacts be excluded?

And thus you need to look all measured frequencies.
A time-over frequency plot results in a graph where you can try to extract features.
The difficulty is here that the data is polluted and that you cannot easily see the same features from the waterfall diagram, because the line weights are to thick.
A solution can be to use a smoothing function (running averages), however, they average over the data, which is often not beneficial.
A second solution is to use dots instead of lines, which might point a different picture.
But, in many cases, a histogram of the data is a powerfull method to find features.
A histogram offers a time-independet look at the data and we can use it to easily measure the noise level by fitting a function to the noise.
With the noise known, the signals can easily be seperated from the noise.

Using a histogram and deriving parameters reduces the dimensionality and complexity of the dataset.
In our example, the classification of the noise condenses hundreds of datapoints into a two numbers (the height and position of the fitted distribution).

### Measurement errors

![Accuracy and precision](images/accuracy-and-precision.png "Accuracy and precision")

Usually you have a hypothesis and need to proof if it is right (or not). Thus, you need to collect data.
However most measurements have errors and some calculations with data introduce errors, like the calculation of an average, which also produces a standard derivation from that average.
Initially the errors are often so large, that you hypothesis cannot be accepted or rejected.
Additionally, the addition of more data will result in sharper error distributions, which will allow to accept or reject the hypothesis with a specific confidence.

* In gerneral: Don't believe data without error bars!
* Use weighted regressions to take the error into account
* Caluclate errors when you aggregate data (i.e. standard distribution)
* Use error propagation, when you derive values with formulas that have inputs with errors.
* Btw: All sensors have measurement errors given by the manufacturer.

![Error propagation](images/error_propagation.png "Error propagation")

### Student Presentations + Q&A (Homework A)

Live Discussion

## Homework B: Complete Analysis of a dataset

- Create a function that uses one column (frequncy) to create a subset of the dataset
- Use `numpy.histogram` to create a histogram from this dataset. Use input parameters according to values derived from the whole dataset.
- Use the histogram data and `scipy.optimize.curve_fit` to fit the histogram to a rayleigh distribution. This distribution needs to be shifted and is used to approximate the noise floor.
- Select the remaining data (the data where the distribution is not defined) and use numpy to calculate it's 50th, 70th and 90th percentile.
- Create a plot containing the raw data, the histogram, the fitted curve and the percentiles.
- Let your function return the frequency, fitting parameters and the percentiles.
- Call this function for every frequency, collect the results, create a dataframe and visualize (plot) the aggregated data.

Hint: Use a `ProcessPoolExecutor` from `concurrent.futures` to parallelize the execution and plot generation.

Hint: Let your function return a dictionary and save the results in a list. Use this list to create a dataframe

![Single frequency](images/single_frequency.png "Single frequency")

## Sprint 3

### Lessons Learned "Study design" (with Jessica)

TBD

### Introduction into sklearn

TBD

### Introduction into deep-learning

* https://i40.fh-aachen.de/courses/datascience/deep-learning

### Student Presentations + Q&A (Task B)

Live Discussion
