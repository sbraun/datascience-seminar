#!/usr/bin/python3
# vim: set fileencoding=utf-8 :

"""
Contains functions responsible for reading and preprocessing data
"""

import logging

logger = logging.getLogger(__name__)


def preprocess(filename):
    """
    this function opens filename, preprocessed the data and returns a preprocessed dataframe
    """
    # TODO
