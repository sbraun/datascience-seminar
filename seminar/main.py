#!/usr/bin/python3
# vim: set fileencoding=utf-8 :

"""
Main function processing the seminar
"""

import argparse
import logging
from logging.config import dictConfig
from pathlib import Path

from seminar.preprocessing import preprocess


logger = logging.getLogger(__name__)


def main():
    """
    The main programm
    """
    df = preprocess(Path('data/measurement.csv'))
    # TODO
    logger.info(df)


def parse_command_line(argv=None):
    """
    Commandline parser
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-q', '--quiet',
        help="Be quiet",
        action="store_const",
        dest="loglevel",
        const=logging.WARNING,
        default=logging.INFO,
    )
    parser.add_argument(
        '-d', '--debug',
        help="Debugging statements",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
    )
    return parser.parse_args(argv)


def get_logger_config(args):
    """
    generator for the logging configuration
    """
    logger_config = {
        'version': 1,
        'disable_existing_loggers': True,
        'formatters': {
            'default': {
                'format': "[%(asctime)s.%(msecs)03d] %(levelname).1s %(message)s",
                'datefmt': "%H:%M:%S",
            },
            'debug': {
                'format': "[%(asctime)s.%(msecs)03d] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
                'datefmt': "%H:%M:%S",
            },
        },
        'handlers': {
            'console': {
                'class': "logging.StreamHandler",
                'level': args.loglevel,
                'formatter': 'debug' if args.loglevel <= logging.INFO else "default",
            },
        },
        'loggers': {
            '__main__': {},
            'seminar': {},
        },
        'root': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
    }
    return logger_config


if __name__ == "__main__":
    # Executed when started from command line (or via python -m)
    arguments = parse_command_line()
    dictConfig(get_logger_config(arguments))
    main()
